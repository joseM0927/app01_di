import Helpers from './helpers.js'
import Modal from './modal.js'

export default class Productos {
    
    #productos;
    #cont

    constructor() {
        this.#productos= [];
        this.#cont= 0;
    }

    static async crear() {
        const instancia = new Productos();

        await Helpers.cargarPagina(
            '#index-contenido', './resources/views/productos.html'
        ).catch(error =>
            Helpers.alertar(
                '#index-contenido',
                'Problemas al acceder al carrito de compras', error
            )
        );
        console.log('Cargada la página de productos');

        instancia.#productos = await Helpers.leerJSON(
            './data/productos.json').catch(error =>
                Helpers.alertar(
                    '#index-contenido',
                    'Problemas al acceder a los productos', error
                )
            );
        console.log('Cargados los productos', instancia.#productos);

        return instancia;
    }


    mostrarProductos(){
        let tablaFichaProducto= '';
        this.#productos.forEach((elemento) => {
            
            tablaFichaProducto= `
                <tr id="ficha-tabla-${elemento.id}">
                    <td id="tabla-id-${elemento.id}" class="border px-4 py-2 text-center">${elemento.id}</td>
                    <td id="tabla-referencia-${elemento.id}" class="border px-4 py-2 text-center">${elemento.referencia}</td>
                    <td id="tabla-precio-${elemento.id}" class="border px-4 py-2 text-center">${elemento.precio}</td>
                    <td class="border px-4 py-2">
                        <div class="flex flex-col">
                            <div id="resumen-producto-tabla" class="overflow-hidden mx-auto text-center">
                                <p id="tabla-resumen-${elemento.id}">${elemento.resumen}</d>     
                            </div>
                            <button type="button" id="btn-verMas-${elemento.id}" class="mx-auto font-semibold">...más</button>
                        </div>
                    </td>
                    <td id="tabla-disponible-${elemento.id}" class="border px-4 py-2 text-center">${elemento.disponible}</td>
                    <td  class="border px-2 py-2">
                        <div>
                            <img src="/resources/assets/images/${elemento.imagen}" class="m-auto" style="max-width: 10rem;">
                        </div>
                    </td>
                    <td class="border px-4 py-2 text-center">
                        <button type="button" id="btn-eliminar-${elemento.id}" class="mx-1"><i class="fas fa-trash"></i></button>
                        <button type="button" id="btn-editar-${elemento.id}" class="mx-1"><i class="fas fa-edit"></i></button>
                    </td>
                </tr>
            `;

            document.querySelector('#cuerpo-tabla-productos').insertAdjacentHTML('beforeend', tablaFichaProducto);
            document.querySelector(`#btn-verMas-${elemento.id}`).addEventListener('click', e => {
                this.mostrarFichaCompleta(elemento);
            });
            document.querySelector(`#btn-eliminar-${elemento.id}`).addEventListener('click', e => {

                let idModal=`modal-eliminar-${elemento.id}`;
                let modalHeader= `<p class="text-lg font-bold text-gray-900 mx-auto mb-2 mt-2">
                    ELIMINAR PRODUCTO</p>`;
                let modalBody= `<p class="text-lg font-semibold text-gray-800">
                    Desea eliminar ${elemento.referencia} de su lista de productos</p>`;
                let modalFooter= `<button type="button" id="btn-confirmar-eliminar-${elemento.id}" 
                class="bg-red-600 text-white rounded mx-1 my-1 p-1">eliminar</button> `;

                Modal.cargarModal(idModal, modalBody, modalHeader, modalFooter);
                document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', e => {
                    Modal.cerrarModal(idModal);
                });
                document.querySelector(`#btn-confirmar-eliminar-${elemento.id}`).addEventListener('click', e => {
                    this.eliminarFicha(elemento);
                    Modal.cerrarModal(idModal);
                });
                
            });
            document.querySelector(`#btn-editar-${elemento.id}`).addEventListener('click', e => {
                this.mostarFichaEditable(elemento);
            });

            
        });

        document.querySelector(`#btn-agregar`).addEventListener('click', e => {
            this.agregarFicha()
        });

        this.#cont+= this.#productos.length+1;

    }
    

    mostrarFichaCompleta(elemento){

        let modalHeader=`  
            <p class="text-xl font-bold text-gray-900 mx-auto">${elemento.referencia}</p>
        `;
        let modalBody=`
            <img src="/resources/assets/images/${elemento.imagen}" class="mx-auto mb-2" style="max-width: 13rem;">
            <p class="text-lg font-semibold mb-2">$${elemento.precio} · Stock: ${elemento.disponible}</p> 
            <p class="text-sm text-gray-700 mb-1">
                ${elemento.resumen}
            </p>
            <p class="text-sm text-gray-700 mb-1">
                ${elemento.detalles}
            </p>
        `;

        let modalId= `${elemento.id}`;


        Modal.cargarModal(modalId, modalBody, modalHeader)

        document.querySelector(`#btn-cerrar-modal-${modalId}`).addEventListener('click', e => {
            Modal.cerrarModal(modalId);
        });
    }


    mostarFichaEditable(elemento){

        let idModal= `editar-${elemento.id}`;
        let modalHeader= `
            <p class="text-xl font-bold text-gray-900 mx-auto">EDITAR PRODUCTO</p>
        `;
        let modalFooter= `
            <button type="button" id="btn-guardarCambio-${elemento.id}" 
            class="bg-blue-600 text-white rounded mx-1 my-1 p-1">guardar</button>
        `;
        let modalBody= `
        <div class="overflow-y-scroll overflow-x-hidden myScroll">
        <form id="form-editar" class="w-full px-4">
            <div class="flex flex-wrap -mx-3 mb-0">
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                        for="edt-id">
                        Identificador
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        id="edt-id" type="text" value="${elemento.id}" disabled>
                </div>
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="edt-referencia">
                        Referencia
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="edt-referencia" type="text" value="${elemento.referencia}">
                </div>
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="edt-disponible">
                        Disponible
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="edt-disponible" type="number" value="${elemento.disponible}">
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-0">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="edt-resumen">
                        Resumen de caracteristicas
                    </label>
                    <textarea
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="edt-resumen" type="text" >${elemento.resumen}</textarea>
                </div>
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="edt-detalles">
                        Detalle de caracteristicas
                    </label>
                    <textarea
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="edt-detalles" type="text">${elemento.detalles}</textarea>
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-2">
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="edt-precio">
                        Precio
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="edt-precio" type="number" value="${elemento.precio}">
                </div>
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="edt-imagen">
                        Imagen del producto
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="edt-imagen" type="text" value="${elemento.imagen}" disabled>
                </div>
                <label
                    class="w-3/12 mx-auto flex flex-col items-center md:w-3/12 px-2 py-2 bg-white text-blue rounded-lg shadow-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-white mt-1">
                    <svg class="w-6 h-6" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path
                            d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                    </svg>
                    <span class="mt-2 text-base leading-normal text-center">Cargar imagen</span>
                    <input type='file' class="hidden" />
                </label>
            </div>
        </form>
        </div>
        `;

        Modal.cargarModal(idModal, modalBody, modalHeader, modalFooter)
        document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', e => {
            Modal.cerrarModal(idModal);
        });
        document.querySelector(`#btn-guardarCambio-${elemento.id}`).addEventListener('click', e => {
            
            let refE= document.querySelector('#edt-referencia').value;
            let disE= parseInt(document.querySelector('#edt-disponible').value, '10');
            let resE= document.querySelector('#edt-resumen').value;
            let detE= document.querySelector('#edt-detalles').value;
            let preE= parseInt(document.querySelector('#edt-precio').value, '10');

            this.editarProducto(elemento.id, refE, disE, resE, detE, preE);
            Modal.cerrarModal(idModal);
        });

    }

    
    eliminarFicha(elemento){
        document.querySelector(`#ficha-tabla-${elemento.id}`).innerHTML=' ';
        console.log(`inicial: ${this.#productos.length}`)
        for (let i=0; i<this.#productos.length; i++){
            if (this.#productos[i].id==elemento.id){
                this.#productos.splice(i,1)
                console.log("eliminado")
            }
        }
        console.log(`final: ${this.#productos.length}`)
    }

    agregarFicha(){

        let idModal= `agregar-producto`;
        let idDestino= '#index-contenido';
        let modalClass= 'flex flex-col bg-white mx-auto mt-4 myModalForm rounded';
        let modalHeader= `
            <p class="text-lg font-bold text-gray-900 mx-auto mb-2 mt-2">
                AGREGAR PRODUCTO
            </p>
        `;
        let modalFooter= `
            <button type="submit" id="btn-crear" 
            class="bg-blue-600 text-white rounded mx-1 my-1 p-1">crear</button>
        `;
        let modalBody= `
        <div class="overflow-y-scroll overflow-x-hidden myScroll">
        <form id="form-agregar" class="w-full px-4">
            <div class="flex flex-wrap -mx-3 mb-0">
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                        for="form-agregar-id">
                        Identificador
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        id="form-agregar-id" type="text" value="PROD0${this.#cont}" disabled>
                </div>
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="form-agregar-referencia">
                        Referencia
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="form-agregar-referencia" type="text" value="" >
                </div>
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="form-agregar-disponible">
                        Disponible
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="form-agregar-disponible" type="number" value="">
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-0">
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="form-agregar-caracteristicas">
                        Resumen de caracteristicas
                    </label>
                    <textarea
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="form-agregar-resumen" type="text" ></textarea>
                </div>
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="form-agregar-detalles">
                        Detalle de caracteristicas
                    </label>
                    <textarea
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="form-agregar-detalles" type="text"></textarea>
                </div>
            </div>
            <div class="flex flex-wrap -mx-3 mb-2">
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="form-agregar-precio">
                        Precio
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="form-agregar-precio" type="number" value="">
                </div>
                <div class="w-full md:w-1/3 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="form-agregar-imagen">
                        Imagen del producto
                    </label>
                    <input
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="form-agregar-imagen" type="text" value=" " disabled>
                </div>
                <label
                    class="w-3/12 mx-auto flex flex-col items-center md:w-3/12 px-2 py-2 bg-white text-blue rounded-lg shadow-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-white mt-1">
                    <svg class="w-6 h-6" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path
                            d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                    </svg>
                    <span class="mt-2 text-base leading-normal text-center">Cargar imagen</span>
                    <input id="form-agregar-imagen-buscar" type='file' class="hidden" />
                </label>
            </div>
        </form>
        </div>
        `;
        Modal.cargarModal(idModal, modalBody, modalHeader, modalFooter)

        document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', e => {
            Modal.cerrarModal(idModal);
        });

        document.querySelector(`#btn-crear`).addEventListener('click', e => {

            let idA= document.querySelector(`#form-agregar-id`).value;
            let refA= document.querySelector(`#form-agregar-referencia`).value;
            let preA= parseInt(document.querySelector(`#form-agregar-precio`).value, '10');
            let resA= document.querySelector(`#form-agregar-resumen`).value;
            let detA= document.querySelector(`#form-agregar-detalles`).value;
            let disA= parseInt(document.querySelector(`#form-agregar-disponible`).value, '10');
            let imgA= 'producto_generico.jpg';

            this.setProductos(idA, refA, preA, resA, detA, disA, imgA)
            document.querySelector('#form-agregar').innerHTML= ' '
            Modal.cerrarModal(idModal)
            
        })

    }

    

    setProductos(Id, Ref, Pre, Res, Det, Dis, Img){
        let producto= new Object();
        producto.id= Id;
        producto.referencia= Ref;
        producto.precio= Pre;
        producto.resumen= Res;
        producto.detalles= Det;
        producto.disponible= Dis;
        producto.imagen= Img

        this.#productos.push(producto);
        this.#cont+=1;
        console.log(`Se creo el producto numero: ${this.#productos.length}`);

        this.agregarNuevaFicha(producto)
    }


    agregarNuevaFicha(producto){
        let nuevaFicha= `
        <tr id="ficha-tabla-${producto.id}">
            <td id="tabla-id-${producto.id}" class="border px-4 py-2 text-center">${producto.id}</td>
            <td id="tabla-referencia-${producto.id}" class="border px-4 py-2 text-center">${producto.referencia}</td>
            <td id="tabla-precio-${producto.id}" class="border px-4 py-2 text-center">${producto.precio}</td>
            <td class="border px-4 py-2">
                <div class="flex flex-col">
                    <div id="resumen-producto-tabla" class="overflow-hidden mx-auto text-center">
                        <p id="tabla-resumen-${producto.id}">${producto.resumen}</d>     
                    </div>
                    <button type="button" id="btn-verMas-${producto.id}" class="mx-auto font-semibold">...más</button>
                </div>
            </td>
            <td id="tabla-disponible-${producto.id}" class="border px-4 py-2 text-center">${producto.disponible}</td>
            <td  class="border px-2 py-2">
                <div>
                    <img src="/resources/assets/images/${producto.imagen}" class="m-auto" style="max-width: 10rem;">
                </div>
            </td>
            <td class="border px-4 py-2 text-center">
                <button type="button" id="btn-eliminar-${producto.id}" class="mx-1"><i class="fas fa-trash"></i></button>
                <button type="button" id="btn-editar-${producto.id}" class="mx-1"><i class="fas fa-edit"></i></button>
            </td>
        </tr>
        `;

        document.querySelector('#cuerpo-tabla-productos').insertAdjacentHTML('afterbegin', nuevaFicha);
        document.querySelector(`#btn-verMas-${producto.id}`).addEventListener('click', e => {
            this.mostrarFichaCompleta(producto);
        });
        document.querySelector(`#btn-eliminar-${producto.id}`).addEventListener('click', e => {
            let idModal=`modal-eliminar-${producto.id}`;
            let modalHeader= `<p class="text-lg font-bold text-gray-900 mx-auto mb-2 mt-2">
                ELIMINAR PRODUCTO</p>`;
            let modalBody= `<p class="text-lg font-semibold text-gray-800">
                Desea eliminar ${producto.referencia} de su lista de productos</p>`;
            let modalFooter= `<button type="button" id="btn-confirmar-eliminar-${producto.id}" 
            class="bg-red-600 text-white rounded mx-1 my-1 p-1">eliminar</button> `;

            Modal.cargarModal(idModal, modalBody, modalHeader, modalFooter);
            document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', e => {
                Modal.cerrarModal(idModal);
            });
            document.querySelector(`#btn-confirmar-eliminar-${producto.id}`).addEventListener('click', e => {
                this.eliminarFicha(producto);
                Modal.cerrarModal(idModal);
            });
        });
        document.querySelector(`#btn-editar-${producto.id}`).addEventListener('click', e => {
            this.mostarFichaEditable(producto);
        });   
    } 

    editarProducto(Id, Ref, Dis, Res, Det, Pre){
        
        for(let i=0; i<this.#productos.length; i++){

            if(Id == this.#productos[i].id){
                this.#productos[i].referencia= Ref;
                this.#productos[i].disponible= Dis;
                this.#productos[i].resumen= Res;
                this.#productos[i].detalles= Det;
                this.#productos[i].precio= Pre;

                document.querySelector(`#tabla-referencia-${this.#productos[i].id}`).innerHTML= `${this.#productos[i].referencia}`;
                document.querySelector(`#tabla-disponible-${this.#productos[i].id}`).innerHTML= `${this.#productos[i].disponible}`;
                document.querySelector(`#tabla-resumen-${this.#productos[i].id}`).innerHTML= `${this.#productos[i].resumen}`;
                document.querySelector(`#tabla-precio-${this.#productos[i].id}`).innerHTML= `${this.#productos[i].precio}`;
                
                break;
            }
        }
    }

}