'use strict';

import Helpers from './helpers.js';
import CarritoDeCompras from './carrito-compras.js';
import Productos from './productos.js';

document.addEventListener('DOMContentLoaded', event => {

    let promesa = Helpers.cargarPagina(
        '#index-header',
        './resources/views/menu.html'
    ).then(
        gestionarOpciones
    ).catch(error => {
        Helpers.alertar('#index-contenido', 
                        'Problemas al acceder al menú principal ', error);
    });

    Helpers.leerJSON('./data/productos.json').then(resultado => {
        resultado.forEach(elemento => console.log(elemento))
    }).catch(error => {
        console.error(`Houston, tenemos un problema: ${error}`);
    });

});



let gestionarOpciones = resultado => {
    let elemento = `#${resultado.id}`; // se asigna '#index-header'

    cargarProductos(elemento);
    cargarCarrito(elemento);
    cargarContactenos(elemento);
    cargarNosotros(elemento);
    cargarCambioPassword(elemento);
    cargarActualizarDatos(elemento);

}


let cargarProductos = elemento => {
    
    let referencia = document.querySelector(
        `${elemento} a[id='menu-productos']`
    );
    
    referencia.addEventListener('click', async (event) => {
        event.preventDefault();
        let pagProductos= await Productos.crear();
        pagProductos.mostrarProductos();
    });
}

let cargarCarrito = elemento => {
    
    let referencia = document.querySelector(
        `${elemento} a[id='menu-carrito']`
     );
 
     referencia.addEventListener('click', async (event) => {
         event.preventDefault();
         let carrito = await CarritoDeCompras.crear();
         carrito.gestionarVentas();
     });

    
}

let cargarContactenos = elemento => {

    let referencia = document.querySelector(
        `${elemento} a[id='menu-contactenos']`
    );

    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido',
                             '/resources/views/contactenos.html').then(async () => {
            let paises= await Helpers.leerJSON('./data/paises.json');
            Helpers.llenarLista('contactenos-paises', paises, 'name', 'code');})
            .catch(error => {
                Helpers.alertar('#index-contenido', 
                                'Problemas al acceder a contáctenos', error);
            })
    });
}

let cargarNosotros = elemento => {

    let referencia = document.querySelector(
        `${elemento} a[id='menu-nosotros']`
    );

    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido',
                             '/resources/views/nosotros.html')
            .catch(error => {
                Helpers.alertar('#index-contenido', 
                                'Problemas al acceder a nosotros', error);
            })
    });
}

let cargarCambioPassword = elemento => {

    let referencia = document.querySelector(
        `${elemento} a[id='menu-cambio-password']`
    );

    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido',
                             '/resources/views/cambio_password.html')
            .catch(error => {
                Helpers.alertar('#index-contenido', 
                                'Problemas al acceder a nosotros', error);
            })
    });
}

let cargarActualizarDatos = elemento => {

    let referencia = document.querySelector(
        `${elemento} a[id='menu-actualizar-datos']`
    );

    referencia.addEventListener('click', (event) => {
        event.preventDefault();
        Helpers.cargarPagina('#index-contenido',
                             '/resources/views/actualizar_datos.html').then(cargarFormularios)
            .catch(error => {
                Helpers.alertar('#index-contenido', 
                                'Problemas al acceder a nosotros', error);
            })
    });
}


async function cargarFormularios(){
    let departamentos= await Helpers.leerJSON('./data/departamentos.json');
    let ciudades= await Helpers.leerJSON('./data/ciudades.json');

    Helpers.llenarLista('usuario-departamento', departamentos, 'nombre', 'codigo');

    document.querySelector('#usuario-departamento').addEventListener('change', e => { 
        let codigoDepartamento= e.target.options[e.target.selectedIndex].value;
        let ciudadesDepartamento= ciudades.filter(objCiudad => objCiudad.codigo.substr(0,2) === codigoDepartamento);
        Helpers.llenarLista('usuario-ciudad', ciudadesDepartamento, 'nombre', 'codigo');
    });

    document.querySelector('#usuario-departamento').dispatchEvent(new MouseEvent('change'));
}




