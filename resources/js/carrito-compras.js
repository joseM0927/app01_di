import Helpers from './helpers.js';
import Modal from './modal.js';

export default class CarritoDeCompras {

    #productos;
    #porComprar;
    #descuento;

    constructor() {
        this.#productos = [];
        this.#porComprar = [];
        this.#descuento = 15;
    }


    static async crear() {
        const instancia = new CarritoDeCompras();

        await Helpers.cargarPagina(
            '#index-contenido', './resources/views/carrito.html'
        ).catch(error =>
            Helpers.alertar(
                '#index-contenido',
                'Problemas al acceder al carrito de compras', error
            )
        );
        console.log('Cargada la página del carrito');

        instancia.#productos = await Helpers.leerJSON(
            './data/productos.json').catch(error =>
                Helpers.alertar(
                    '#index-contenido',
                    'Problemas al acceder a los productos', error
                )
            );
        console.log('Cargados los productos', instancia.#productos);

        return instancia;
    }

    
    gestionarVentas() {
        this.#productos.forEach( (producto, indice) => {
            let idEnlace= `carrito-producto-${indice}`;
            
            let fichaProducto= `
                <div class="bg-white px-3 py-2 rounded-lg mb-2 shadow-lg" style="max-width: 25rem;">
                    <div id="carrito-card-${producto.id}" class="flex flex-col">
                        <img src="/resources/assets/images/${producto.imagen}">
                        <p class="text-lg font-bold text-gray-700 my-1">${producto.referencia} · $${producto.precio}</p>
                        <p class="text-sm  text-gray-600 my-1">${producto.resumen}</p>
                        
                        <a id="${idEnlace}" data-indice="${indice}" href="#carrito-elegidos"
                                class=" bg-teal-600 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded
                                        focus:outline-none focus:shadow-outline text-center"
                                        style"">AGREGAR AL CARRITO
                        </a>
                    </div>
                </div>
            `;

            document.querySelector('#carrito-disponibles').insertAdjacentHTML('beforeend', fichaProducto);
            document.querySelector(`#${idEnlace}`).addEventListener('click', e => {
                this.agregaAlCarrito(e.target.dataset.indice);
            })
        });
        this.cargarMejoresOfertas();
        let btnPagar = document.querySelector('#carrito-btnpagar');
        btnPagar.style.display = 'none';
        btnPagar.addEventListener('click', event => this.procesarPago());
    }


    cargarMejoresOfertas(){
        
        document.querySelector(`#slide`).innerHTML= `
            <img src="/resources/assets/images/${this.#productos[0].imagen}" 
            class="h-full rounded-l-lg  sm:w-auto" style="width:35%">
            <div class="flex flex-col text-white  text-base sm:text-lg md:text-xl xl:text-3xl  mx-auto justify-center font-bold">
                <p class="text-center">${this.#productos[0].referencia}</p>
                <p class="text-center">$${this.#productos[0].precio}</p>
            </div>
        `;
        
        let boton=` `;
        let contador=1;

        for(let i=0; i<6; i++){
            
            boton = `<button type=button id="slide-${this.#productos[i].id}" 
            class="w-8 mr-1 h-8 text-gray-700 rounded-full bg-white flex justify-center items-center">
            ${contador}</button>`;

            document.querySelector('#botones-slider').insertAdjacentHTML('beforeend', boton);

            contador++;

            document.querySelector(`#slide-${this.#productos[i].id}`).addEventListener('click', e => {
                document.querySelector(`#slide`).innerHTML= `
                <img src="/resources/assets/images/${this.#productos[i].imagen}" 
                class="h-full rounded-l-lg sm:w-auto" style="width:35%">
                <div class="flex flex-col text-white  text-base sm:text-lg md:text-xl xl:text-3xl  mx-auto justify-center font-bold">
                    <p class="text-center">${this.#productos[i].referencia}</p>
                    <p class="text-center">$${this.#productos[i].precio}</p>
                </div> `;
            });
        }
    }


    agregaAlCarrito(indice) {

        let idBtnEliminar = `carrito-btneliminar-${indice}`;
        let disponibles = this.#productos[indice].disponible;
        let item = this.#porComprar.find(producto => producto.indice === indice);

        if (item) {
            document.querySelector(`#carrito-venta-${item.indice}`).scrollIntoView();
            document.querySelector(`#lstcantidad-${item.indice}`).focus();
            return;
        }

        this.#porComprar.push({
            indice,
            cantidad: 1,
            precio: this.#productos[indice].precio
        });

        let elementosLista = '<option>1</option>';
        for (let i = 2; i <= disponibles; i++) {
            elementosLista += `<option>${i}</option>`;
        }
        
        let producto = `
            <div id="carrito-venta-${indice}"
                class="bg-white border rounded flex p-4 justify-between items-center flex-wrap" style="max-width: 27rem;">
                <div class="w-2/4">
                    <h3 class="text-lg font-medium">${this.#productos[indice].referencia}</h3>
                    <h4 class="text-red-700 text-xs font-bold mt-1">Sólo quedan ${disponibles} en stock </h4>
                </div>
                <div>
                    <h5 id="precio-seleccion-${indice}" class="text-2xl font-medium">
                        <sup class="text-lg text-teal-600">$</sup>
                        ${this.#productos[indice].precio}
                    </h5>
                    <h5 class="text-sm font-bold text-teal-800">Descuento ${this.#descuento}%</h5>
                </div>
                <div class="w-full flex justify-between mt-4">

                    <button id="${idBtnEliminar}" data-indice="${indice}" 
                            class="text-red-700 hover:bg-red-100 px-2">ELIMINAR</button>

                    <label class="block uppercase tracking-wide text-gray-700"
                        for="grid-first-name">
                        UNIDADES
                        <select id="lstcantidad-${indice}"
                            class="ml-3 text-sm bg-teal-700 border border-teal-200 text-white p-2 rounded leading-tight">
                            ${elementosLista}
                        </select>
                    </label>
                </div>
            </div>
        `;
        
        document.querySelector('#carrito-elegidos').insertAdjacentHTML('beforeend', producto);
        document.querySelector('#carrito-btnpagar').style.display = '';

        document.querySelector(`#${idBtnEliminar}`).addEventListener('click', e => {
            this.eliminarDelCarrito(e.target.dataset.indice);
        });

        document.querySelector('#carrito-btnvaciar').addEventListener('click', e => {
            this.vaciarCarrito();
        });

        document.querySelector(`#lstcantidad-${indice}`).addEventListener('change', e => {
            
            document.querySelector(`#precio-seleccion-${indice}`).innerHTML=`
                <sup class="text-lg text-teal-600">$</sup>
                ${e.target.value*this.#productos[indice].precio}
            `;
            this.#porComprar.forEach(producto => {
                if(producto.indice==indice){
                    producto.cantidad= e.target.value;
                }
            })
        })
        
    }


    vaciarCarrito(){
        let elegidos = document.querySelector(`#carrito-elegidos`);
        this.#porComprar.forEach(elemento => {
            elegidos.removeChild(document.querySelector(`#carrito-venta-${elemento.indice}`));
        })

        this.#porComprar= [ ];
        
        if (this.#porComprar.length === 0) {
            document.querySelector('#carrito-btnpagar').style.display = 'none';
        }
        
        
    }


    eliminarDelCarrito(indice) {
        // eliminar la ficha de la lista de compras
        let elemento = document.querySelector(`#carrito-venta-${indice}`);
        elemento.parentNode.removeChild(elemento); // distinto a dejarlo vacío

        // eliminar el elemento del array de los productos a comprar 
        let item = this.#porComprar.find(
           producto => producto.indice === indice
        );
        let i = this.#porComprar.indexOf(item);
        this.#porComprar.splice(i, 1);


        // si no quedan elementos por comprar ocultar el botón de pago
        if (this.#porComprar.length === 0) {
            document.querySelector('#carrito-btnpagar').style.display = 'none';
        }

    }


    procesarPago() {
        
        let valor = 0;
        let resumenProductos= ` `;
        let temporal= JSON.parse(JSON.stringify(this.#porComprar));
        
        temporal.forEach(elemento => {
            resumenProductos += `<p class="text-base text-orange-900 font-bold">${this.#productos[elemento.indice].referencia} - ${elemento.cantidad} unds - $ ${elemento.precio*elemento.cantidad}</p> `;
            valor += elemento.precio*elemento.cantidad;
        });

        let iva = 0.19*valor;
        let totalPago = valor + iva;

        let pago = `
            <div id="resumen-pago" class="bg-white rounded shadow p-2 mt-4" style="max-width: 27rem;">
                <div class="w-full bg-orange-200 px-8 py-2">
                    <div class="flex flex-row justify-between">
                        <h3 class="text-2xl mt-2 font-bold">Resumen del pago</h3>
                        <button type="button" id="btn-cerrar-resumenPago" class="text-2xl mt-2 font-bold">X</button>
                    </div>
                    <div class="flex flex-col justify-between mt-3">
                        ${resumenProductos}
                    </div>
                    <div class="bg-orange-300 h-1 w-full mt-2"></div>
                    <div class="flex justify-between mt-2">
                       <div class="text-lg text-orange-900 font-bold">Valor</div>
                       <div class="text-lg text-right font-bold">$${valor}</div>
                    </div>
                    <div class="flex justify-between mt-2">
                        <div class="text-lg text-orange-900 font-bold">
                             IVA (19%)
                        </div>
                        <div class='text-lg text-right font-bold'>$${iva}</div>
                    </div>
                    <div class="bg-orange-300 h-1 w-full mt-2"></div>
                    <div class="flex justify-between mt-2">
                        <div class="text-xl text-orange-900 font-bold">
                             Total
                        </div>
                        <div class="text-xl text-orange-900 font-bold">
                             $${totalPago}
                        </div>
                    </div>
                    <button id="carrito-btnconfirmar"
                        class="px-2 py-2 bg-teal-600 text-white w-full mt-2 mb-2
                        rounded shadow font-bold hover:bg-teal-800">
                        CONFIRMAR
                    </button>
                </div>
            </div>
        `;
        
        document.querySelector('#carrito-confirmacion').innerHTML = pago;
        document.querySelector('#btn-cerrar-resumenPago').addEventListener('click', e => {
            document.querySelector('#carrito-confirmacion').removeChild(
                document.querySelector('#resumen-pago')
            )
        });
        document.querySelector('#carrito-btnconfirmar')
           .addEventListener('click', event => { 
               this.renovarStock(temporal);
               this.confirmarPago()
        });
    }


    renovarStock(comprados){
        let mensaje=`<p class="text-lg font-bold text-red-500 mt-0">Sin unidades disponibles</p>`;
        comprados.forEach(elemento => {
            this.#productos[elemento.indice].disponible -= elemento.cantidad; 
            if(this.#productos[elemento.indice].disponible <= 0){
                document.querySelector(`#carrito-card-${this.#productos[elemento.indice].id}`).removeChild(
                    document.querySelector(`#carrito-producto-${elemento.indice}`)
                );
                document.querySelector(`#carrito-card-${this.#productos[elemento.indice].id}`).insertAdjacentHTML(
                    'beforeend', mensaje);
            }
        });

    }
    

    confirmarPago() {
        if (Helpers.existeElemento('#modal-orden-envio')) return;
       
        let nroOrden = Helpers.getRandomInt(10000, 9999999);
        let idModal= 'modal-orden-envio';
        let modalHeader= `<p class="text-2xl font-bold text-purple-600 mx-auto">COMPRA EXITOSA</p>`;
        let modalBody = `
            <div class="flex flex-col justify-center"> 
                <div class="">
                    <h3 class="text-2xl mt-4 font-bold text-teal-900">
                        Gracias por su compra!!!
                    </h3>

                    <h4 id="carrito-nro-orden" class="text-sm text-gray-600 font-bold">
                        ORDEN DE ENVÍO #${nroOrden}
                    </h4>
                </div>

                <img src="https://image.flaticon.com/icons/svg/1611/1611768.svg"
                     alt="" class="w-24 mx-auto">
            </div>
        `;
        

        Modal.cargarModal(idModal, modalBody, modalHeader);
        

        document.querySelector(`#btn-cerrar-modal-${idModal}`).addEventListener('click', e => { 
            Modal.cerrarModal(idModal);
            
            document.querySelector('#carrito-confirmacion').removeChild(
                document.querySelector('#resumen-pago')
            );
            this.vaciarCarrito();
        });
    }

    
}


