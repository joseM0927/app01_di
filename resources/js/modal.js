export default class Modal {

    
    static async cargarModal(id, modalBody=' ', modalHeader='', modalFooter='', modalClass=''){
        
        let myModal= `
            <div id="modal-${id}" class="bg-white border rounded-lg text-center mx-auto mt-4 p-1" style="max-width: 30rem;">
                <div class="mb-1">
                    ${modalHeader}
                </div>
                <div class="bg-gray-300 w-full h-1"></div>
                <div class="myScroll items-center my-2" style="overflow-y: scroll; max-height: 70vh;">
                    ${modalBody}
                </div>
                <div class="bg-gray-300 w-full h-1"></div>
                <div class="mt-1">
                    ${modalFooter}
                    <button type="button" id="btn-cerrar-modal-${id}" 
                    class="bg-green-700 text-white rounded mx-1 my-1 p-1">cerrar</button>
                <div>
            </div>
                
        `;

        document.querySelector(`#modal-container`).insertAdjacentHTML('afterbegin', myModal);
        document.querySelector(`#modal-container`).classList.remove('invisible');
        document.querySelector('#cuerpo-principal').classList.add('overflow-hidden');
    }

    static async cerrarModal(id){
        document.querySelector(`#modal-container`).removeChild(document.querySelector(`#modal-${id}`));
        document.querySelector(`#modal-container`).classList.add('invisible');
        document.querySelector('#cuerpo-principal').classList.remove('overflow-hidden');
    }

    


}